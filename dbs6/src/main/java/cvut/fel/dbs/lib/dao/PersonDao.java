package cvut.fel.dbs.lib.dao;

import cvut.fel.dbs.lib.model.Person;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Class PersonDao.
 * The Data Access Object, a manager working with database, contains methods for interactions with db
 */
public class PersonDao {
    private EntityManager em;

    /**
     * Instantiates a new Person dao.
     */
    public PersonDao() {
    }

    /**
     * Instantiates a new Person dao with given EntityManager class object
     *
     * @param em the object of EntityManager class
     */
    public PersonDao(EntityManager em) {
        this.em = em;
    }

    /**
     * Find all persons in database, returns a list of objects with type Person.
     *
     * @return the list
     */
    public List<Person> findAll(){
        try{
            return em.createQuery("SELECT p FROM Person p", Person.class).getResultList();
        }
        catch (NullPointerException ex) {
            return null;
        }
    }

    /**
     * Creates a new object with type Person, adds the given person to database
     *
     * @param p the person
     */
    public void create(Person p){
        em.persist(p);
    }

    /**
     * Finds a person with given id.
     *
     * @param id the id
     * @return the person
     */
    public Person find(Long id){
        try {
            return em.find(Person.class, id);
        } catch (Exception e) {
            return null;
        }


    }

    /**
     * Updates a person in database.
     *
     * @param p the person
     * @return the updated person
     */
    public Person merge(Person p){
        return em.merge(p);
    }

    /**
     * Deletes a person from database.
     *
     * @param p the person
     */
    public void delete(Person p){
        em.remove(p);
    }
}
