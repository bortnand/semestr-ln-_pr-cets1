package cvut.fel.dbs.lib;

import cvut.fel.dbs.lib.dao.PersonDao;
import cvut.fel.dbs.lib.dao.PostDao;
import cvut.fel.dbs.lib.model.Person;
import cvut.fel.dbs.lib.model.Post;

import cvut.fel.dbs.lib.view.NewJFrame;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.persistence.*;

import static cvut.fel.dbs.lib.view.NewJFrame.*;


/**
 * The main class Application, which contains all main functions in GUI and setups
 */
public class Application {

    private static EntityManagerFactory emf;
    private static EntityManager em;
    public DefaultTableModel model;
    public PersonDao personDao;
    public PostDao postDao;

    public List<Person> personList;

    public List<Post> postList;

    public DateFormat df = new SimpleDateFormat("dd/MM/yy ");

    public void run(){
        setup();
        workWithDB();
    }

    /**
     * the function sets up connection and creates new instances personDao and postDao, finds lists of authors and posts
     */
    public void setup(){

        // create emf
        emf = Persistence.createEntityManagerFactory("pu");
        // create entity manager
        em = emf.createEntityManager();
        // create Person dao and Post dao
        personDao = new PersonDao(em);
        postDao = new PostDao(em);

        em.getTransaction().begin();
        personList = personDao.findAll();
        postList = postDao.findAll();
        em.getTransaction().commit();
    }


    /**
     * Closes connection
     */
    public static void close(){
        em.close();
        emf.close();
    }


    /**
     * Shows all users.
     */
    public  void showUsers() {
        setup();
        model = (DefaultTableModel) NewJFrame.tableUsers.getModel();
        while (model.getRowCount()>0)
        {
            model.removeRow(0);
        }
        
        Object[] row = new Object[5];

        for (Person person : personList) {
            row[0] = person.getId();
            row[1] = person.getName();
            row[2] = person.getEmail();
            row[3] = person.getProfession();
            row[4] = person.getHashedPassword();
            model.addRow(row);
        }
    }

    /**
     * Shows all posts.
     */
    public void showPosts() {
        DefaultTableModel model = (DefaultTableModel) tablePosts.getModel();
        refreshTablePost(model);
    }

    /**
     * Show all posts tab2.
     */
    public  void showPosts2() {
        DefaultTableModel model = (DefaultTableModel) tablePosts2.getModel();
        refreshTablePost(model);
    }

    /**
     * Adds a new post, inserts into database and shows updated table
     */
    public  void addNewPost(Post post, DefaultTableModel model) {

        setup();

        if (post.getTitle().equals("")) {
            showDialog("Title must not be empty");
        } else if (post.getText().equals("")) {
            showDialog("Text must not be empty");
        } else if (!post.getTitle().matches("^[a-zA-Z0-9_ ]{3,30}$")){
            showDialog("Title contains unsupported symbols or has wrong length(length must be 3 to 30)");
        } else if (!post.getText().matches("^[a-zA-Z0-9_ ]{10,500}$")) {
            showDialog("Text contains unsupported symbols or has wrong length(length must be 10 to 500)");
        } else if (!(post.getCategory().isEmpty()) && !post.getCategory().matches("^[a-zA-Z0-9_ ]{3,20}$")) {
            showDialog("Category contains unsupported symbols or has wrong length(length must be 3 to 20)");
        }

        else {
            em.getTransaction().begin();
            postDao.create(post);
            postList = postDao.findAll();
            em.getTransaction().commit();
            refreshTablePost(model);
            showDialog("Post has been added");
            titlePost_input.setText("");
            textPost_input.setText("");
            idPost_input.setText("");
            datePost_input.setText("");
            categoryPost_input.setText("");
        }
    }

    /**
     * Shows message dialog with given string
     * @param value message
     */
    public void showDialog(String value) {
        JOptionPane.showMessageDialog(null,value);
    }

    /**
     * Deletes the post, also deletes the post in database, shows updated table
     */
    public void deletePost() {

        model = (DefaultTableModel) tablePosts.getModel();
        setup();
        String str= idPost_input.getText();
        long id = Long.parseLong(str);

        try {
            em.getTransaction().begin();
            Post deletedPost = postDao.find(id);
            if (!deletedPost.getAuthors().isEmpty()) {
                for (Person author : deletedPost.getAuthors()) {
                    author.getAuthoredPosts().remove(deletedPost);
                    personDao.merge(author);
                }
            }
            postDao.delete(deletedPost);
            em.getTransaction().commit();

            showDialog("Post has been deleted");
            titlePost_input.setText("");
            textPost_input.setText("");
            idPost_input.setText("");
            datePost_input.setText("");
            categoryPost_input.setText("");
        } catch (Exception e) {
            showDialog("Post with given id doesn't exist");
        }

        refreshTablePost(model);
    }
    public void deletePerson() {

        setup();
        String str= userID_input1.getText();
        long id = Long.parseLong(str);

        try {
            em.getTransaction().begin();
            Person deletedPerson = personDao.find(id);
            if (!deletedPerson.getAuthoredPosts().isEmpty()) {
                for (Post post : deletedPerson.getAuthoredPosts()) {
                    post.getAuthors().remove(deletedPerson);
                    postDao.merge(post);
                }
            }
            personDao.delete(deletedPerson);
            em.getTransaction().commit();

            showDialog("Person has been deleted");
        } catch (Exception e) {
            showDialog("Person with given id doesn't exist");
        }

    }

    /**
     * Updates the chosen post, shows updated table
     */
    public  void updatePost(DefaultTableModel model) {
        setup();
        String str= idPost_input.getText();
        long id = Long.parseLong(str);

        try {
            em.getTransaction().begin();
            Post updatedPost = postDao.find(id);
            if (titlePost_input.getText().equals("")) {
                showDialog("Title must not be empty");
            } else if (textPost_input.getText().equals("")) {
                showDialog("Text must not be empty");
            } else if (!titlePost_input.getText().matches("^[a-zA-Z0-9_ ]{3,30}$")){
                showDialog("Title contains unsupported symbols or has wrong length(length must be 3 to 30)");
            } else if (!textPost_input.getText().matches("^[a-zA-Z0-9_ ]{10,500}$")) {
                showDialog("Text contains unsupported symbols or has wrong length(length must be 10 to 500)");
            } else if (!(categoryPost_input.getText().isEmpty()) && !categoryPost_input.getText().matches("^[a-zA-Z0-9_ ]{3,20}$")) {
                showDialog("Category contains unsupported symbols or has wrong length(length must be 3 to 20)");
            } else {
                updatedPost.setTitle(titlePost_input.getText());
                updatedPost.setText(textPost_input.getText());
                updatedPost.setCategory(categoryPost_input.getText());
                postDao.merge(updatedPost);
                em.getTransaction().commit();
                refreshTablePost(model);
                showDialog("Post has been updated");
                titlePost_input.setText("");
                textPost_input.setText("");
                idPost_input.setText("");
                datePost_input.setText("");
                categoryPost_input.setText("");
            }
        } catch (Exception e ){
            showDialog("Post with given id doesn't exist");
        }
    }


    /**
     * Refreshes the table
     * @param model model of a table
     */
    public void refreshTablePost(DefaultTableModel model) {
        Object[] row = new Object[5];
        while (model.getRowCount()>0)
        {
            model.removeRow(0);
        }
        setup();
        for (Post post : postList) {
            row[0] = post.getId();
            row[1] = post.getTitle();
            row[2] = post.getText();
            row[3] = post.getDate();
            row[4] = post.getCategory();
            model.addRow(row);
        }
    }

    /**
     * Show authored posts of the chosen author.
     */
    public  void showAuthoredPosts(){
        DefaultTableModel model = (DefaultTableModel) tablePosts2.getModel();
        Object[] row = new Object[5];
        String str = userID_input1.getText();
        long id = Long.parseLong(str);
        try {
            em.getTransaction().begin();
            Person current = personDao.find(id);
            em.getTransaction().commit();
            while (model.getRowCount()>0)
            {
                model.removeRow(0);
            }
            for (Post post: current.getAuthoredPosts()) {
                row[0] = post.getId();
                row[1] = post.getTitle();
                row[2] = post.getText();
                row[3] = post.getDate();
                row[4] = post.getCategory();
                model.addRow(row);
            }
        } catch (Exception e) {
            showDialog("Person with given id doesn't exist");
        }
    }

    /**
     * Show authors of the chosen post.
     */
    public void showAuthors() {
        DefaultTableModel model = (DefaultTableModel) tableUsers.getModel();
        Object[] row = new Object[5];

        String str = postID_input2.getText();
        long id = Long.parseLong(str);
        try {
            em.getTransaction().begin();
            Post current = postDao.find(id);
            em.getTransaction().commit();
            while (model.getRowCount()>0)
            {
                model.removeRow(0);
            }
            for (Person p: current.getAuthors()) {
                row[0] = p.getId();
                row[1] = p.getName();
                row[2] = p.getEmail();
                row[3] = p.getProfession();
                row[4] = p.getHashedPassword();
                model.addRow(row);
            }
        } catch (Exception e) {
            showDialog("Post with given id doesn't exist");
        }

    }

    /**
     * Adds the post to an author, creates a relationship between author
     * and post so the chosen author will have the chosen post in authoredPosts
     * and the chosen post will have this author in authors
     */
    public  void addPostToAnAuthor() {
        String str = userID_input1.getText();
        String str2 = postID_input1.getText();
        long id = Long.parseLong(str);
        long id2 = Long.parseLong(str2);

        em.getTransaction().begin();
        Post currentPost = postDao.find(id2);
        Person currentPerson = personDao.find(id);
        if (currentPerson == null || currentPost == null) {
            showDialog("Author or post not found");
        } else {
            Set<Post> listPost = currentPerson.getAuthoredPosts();
            Set<Person> listPerson = currentPost.getAuthors();
            listPerson.add(currentPerson);
            listPost.add(currentPost);
            postDao.merge(currentPost);
            personDao.merge(currentPerson);
            em.getTransaction().commit();
        }
    }

    /**
     * Deletes relation between author and post from author's side
     */
    public  void deleteRelation() {
        String str = userID_input1.getText();
        String str2 = postID_input1.getText();
        long id = Long.parseLong(str);
        long id2 = Long.parseLong(str2);
        em.getTransaction().begin();
        Post currentPost = postDao.find(id2);
        Person currentPerson = personDao.find(id);
        if (currentPerson == null || currentPost == null) {
            showDialog("Author or post not found");
            em.getTransaction().commit();
        } else {
            currentPerson.getAuthoredPosts().remove(currentPost);
            personDao.merge(currentPerson);

            currentPost.getAuthors().remove(currentPerson);
            postDao.merge(currentPost);
            em.getTransaction().commit();
        }
    }

    /**
     * Deletes relation between post and author from post's side.
     */
    public void deleteRelation2() {
        String str = userID_input2.getText();
        String str2 = postID_input2.getText();
        long id = Long.parseLong(str);
        long id2 = Long.parseLong(str2);
        em.getTransaction().begin();
        Post currentPost = postDao.find(id2);
        Person currentPerson = personDao.find(id);
        if (currentPerson == null || currentPost == null) {
            showDialog("Author or post not found");
            em.getTransaction().commit();
        } else {
            Set<Post> listPost = currentPerson.getAuthoredPosts();
            Set<Person> listPerson = currentPost.getAuthors();
            listPerson.remove(currentPerson);
            personDao.merge(currentPerson);
            listPost.remove(currentPost);
            postDao.merge(currentPost);
            em.getTransaction().commit();
        }
    }

    /**
     * Adds a author to a post, creates a new relationship between
     * post and author from post's side
     */
    public void addAuthorToAPost() {

        String str = userID_input2.getText();
        String str2 = postID_input2.getText();
        long id = Long.parseLong(str);
        long id2 = Long.parseLong(str2);

        em.getTransaction().begin();
        Post currentPost = postDao.find(id2);
        Person currentPerson = personDao.find(id);
        if (currentPerson == null || currentPost == null) {
            showDialog("Author or post not found");
            em.getTransaction().commit();
        } else {
            Set<Post> listPost = currentPerson.getAuthoredPosts();
            Set<Person> listPerson = currentPost.getAuthors();
            listPerson.add(currentPerson);
            listPost.add(currentPost);
            postDao.merge(currentPost);
            personDao.merge(currentPerson);
            em.getTransaction().commit();
        }
    }


    public void setEmf(EntityManagerFactory emf) {
        Application.emf = emf;
    }

    public void setEm(EntityManager em) {
        Application.em = em;
    }

    /**
     * Working with db, creates new persons and posts if needed
     */
    public void workWithDB(){

//
//        for(Post p : postList){
//            LOG.info(p.toString());
//        }

//        Set<Post> listPost = new HashSet<>();
//        Set<Person> listAuthor = new HashSet<>();
//
//
//        for (int i = 0; i < 5; i++){
//            Person p = new Person();
//            p.setName("User1");
//            p.setEmail("asadf@ff.ff");
//            p.setProfession("Programmer");
//            p.setHashedPassword("1d8f3272b013387bbebcbedb4758586d");
//
//            em.getTransaction().begin();
//            personDao.create(p);
//            em.getTransaction().commit();
//            listAuthor.add(p);
//        }
//        for (long i =0; i < 5; i ++){
//            Post p = new Post();
//            p.setTitle("Post1oo");
//            p.setText("fvsvslkjbvslkjfbvlkjbkjbrvkl wlkjrbv lkvb wlkfbvf");
//            p.setDate(df.format(new Date()));
//            p.setCategory("News");
//            em.getTransaction().begin();
//            postDao.create(p);
//            em.getTransaction().commit();
//            listPost.add(p);
//        }
////
//        Person p = new Person();
//        p.setName("UserWithPosts");
//        p.setEmail("asadf@ff.ff");
//        p.setProfession("Programmer");
//        p.setHashedPassword("1d8f3272b013387bbebcbedb4758586d");
//
//        em.getTransaction().begin();
//        personDao.create(p);
//        p.setAuthoredPosts(listPost);
//        personDao.merge(p);
//        em.getTransaction().commit();
//
//        Post post = new Post();
//        post.setTitle("PostWithAuthors");
//        post.setText("fvsvslkjbvslkjfbvlkjbkjbrvkl wlkjrbv lkvb wlkfbvf");
//        post.setDate(df.format(new Date()));
//        post.setCategory("News");
//        em.getTransaction().begin();
//        postDao.create(post);
//        post.setAuthors(listAuthor);
//        postDao.merge(post);
//        for (Post post1 : listPost) {
//            post1.getAuthors().add(p);
//        }
//        em.getTransaction().commit();
    }

    public void setPersonDao(PersonDao personDao) {
        this.personDao = personDao;
    }

    public EntityManagerFactory getEmf() {
        return emf;
    }

    public EntityManager getEm() {
        return em;
    }

    public void setPostDao(PostDao postDao) {
        this.postDao = postDao;
    }

    public PersonDao getPersonDao() {
        return personDao;
    }

    public PostDao getPostDao() {
        return postDao;
    }

    /**
     * The entry point of application.
     * Starts an application
     * @param args the input arguments
     */
    public static void main(String[] args) {

        new Application().run();
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NewJFrame().setVisible(true);
            }
        });
        close();
    }
}
