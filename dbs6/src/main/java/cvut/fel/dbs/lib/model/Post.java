package cvut.fel.dbs.lib.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Class Posts, which describes entity Post, contains variables as columns: id, title,text,date,category
 * and a Set of authors, which describes a ManyToMany relationship with class Person
 */
@Entity
public class Post {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String title;
    @Column(nullable = false)
    private String text;
    
    

    @Column(updatable = false)
    private String date;
    
    private String category;


    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "post_authors",
            joinColumns = {@JoinColumn(name = "post_id")},
            inverseJoinColumns = {@JoinColumn(name = "author_id")}
    )
    private Set<Person> authors = new HashSet<>();


    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Sets category.
     *
     * @param category the category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * Gets category.
     *
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title the title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * Sets text.
     *
     * @param text the text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Gets authors.
     *
     * @return the Set of authors with type Person
     */
    public Set<Person> getAuthors() {
        return authors;
    }

    /**
     * Sets authors.
     *
     * @param authors the Set of authors with type Person
     */
    public void setAuthors(Set<Person> authors) {
        this.authors = authors;
    }

    public String toString() {
        return "Person{" +
                "id=" + id +
                ", title='" + title +
                ", text='" + text + '\'' +
                '}';
    }

}
