package cvut.fel.dbs.lib.dao;

import cvut.fel.dbs.lib.model.Post;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Class PostDao
 * The Data Access Object, a manager working with database, contains methods for interactions with db
 */
public class PostDao {

    private EntityManager em;

    /**
     * Instantiates a new Post dao.
     */
    public PostDao() {
    }

    /**
     * Instantiates a new Post dao with given EntityManager class object
     *
     * @param em the object of EntityManager class
     */
    public PostDao(EntityManager em) {
        this.em = em;
    }

    /**
     * Find all posts in database, returns a list of objects with type Post.
     *
     * @return the list
     */
    public List<Post> findAll(){
        try{
            return em.createQuery("SELECT post FROM Post post", Post.class).getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Creates a new object with type Post, adds the given post to database
     *
     * @param p the post
     */
    public void create(Post p){
        em.persist(p);
    }

    /**
     * Finds a post with given id.
     *
     * @param id the id
     * @return the post
     */
    public Post find(Long id){
        try{
            return em.find(Post.class, id);
        } catch (Exception e) {
            return null;
        }

    }

    /**
     * Updates a post in database
     *
     * @param p the post
     * @return the updated post
     */
    public Post merge(Post p){
        return em.merge(p);
    }

    /**
     * Deletes a post from database.
     *
     * @param p the post
     */
    public void delete(Post p){
        em.remove(p);
    }


}
