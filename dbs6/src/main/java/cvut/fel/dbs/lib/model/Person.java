package cvut.fel.dbs.lib.model;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Class Person, which describes entity Person, contains variables as columns: id, name,email,profession,hashedPassword
 *  * and a Set of authored posts, which describes a ManyToMany relationship with class Post
 */
@Entity
public class Person {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String email;

    private String profession;

    private String hashedPassword;

    @ManyToMany(mappedBy = "authors", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Set<Post> authoredPosts =new HashSet<>();


    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Sets profession.
     *
     * @param profession the profession
     */
    public void setProfession(String profession) {
        this.profession = profession;
    }

    /**
     * Sets hashed password.
     *
     * @param hashedPassword the hashed password
     */
    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Gets profession.
     *
     * @return the profession
     */
    public String getProfession() {
        return profession;
    }

    /**
     * Gets hashed password.
     *
     * @return the hashed password
     */
    public String getHashedPassword() {
        return hashedPassword;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets authored posts.
     *
     * @return the Set of authored posts with type Post
     */
    public Set<Post> getAuthoredPosts() {
        return authoredPosts;
    }

    /**
     * Sets authored posts.
     *
     * @param list the Set of authored posts with type Post
     */
    public void setAuthoredPosts(Set<Post> list) {
        this.authoredPosts = list;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
