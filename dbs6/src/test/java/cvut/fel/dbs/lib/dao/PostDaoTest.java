package cvut.fel.dbs.lib.dao;

import cvut.fel.dbs.lib.model.Post;
import org.junit.Before;
import org.junit.Test;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;


public class PostDaoTest {


    private static EntityManagerFactory emf;
    private EntityManager em;
    private PostDao postDao;
    private Post postExist;
    private Post postNoExist;
    private List<Post> list;
    TypedQuery<Post> query;


    @Before
    public void setup() {
        query = mock(TypedQuery.class);
        em = mock(EntityManager.class);
        postDao = new PostDao(em);
        postExist = new Post();
        postExist.setTitle("some title");
        postExist.setText("some text");
        postNoExist = new Post();
        postNoExist.setId((long)3232);
        postNoExist.setText("fff");
        postNoExist.setTitle("ddd");

        when(em.find(Post.class, postExist.getId())).thenReturn(postExist);
        doNothing().when(em).persist(postExist);
        when(em.find(Post.class,postNoExist.getId())).thenThrow(IllegalArgumentException.class);
    }
    //unit test with mockito
    @Test
    public void createTestWithMockedEm_postGiven_success() {
        //arrange
        Post post1;

        //act
        postDao.create(postExist);
        post1 = em.find(Post.class, postExist.getId());

        //assert
        verify(em).persist(postExist);
        assertNotNull(post1);
        assertEquals(postExist.getText(),post1.getText());
        assertEquals(postExist.getTitle(),post1.getTitle());
        assertEquals(postExist.getId(),post1.getId());
    }
    //unit test with mockito
    @Test
    public void deleteTestWithMockedEm_postDelete_success() {
        //arrange
        Post post1;
        //act and assert
        post1 = em.find(Post.class, postExist.getId());
        assertNotNull(post1);

        postDao.delete(post1);
        verify(em).remove(postExist);
        when(em.find(Post.class, postExist.getId())).thenReturn(null);
        assertNull(em.find(Post.class, postExist.getId()));
    }
    //unit test with mockito
    @Test
    public void mergeTestWithMockedEm_postMerge_success() {
        //arrange
        Post post1;
        String expected = "changed text";

        //act
        post1 = em.find(Post.class, postExist.getId());
        post1.setText(expected);
        postDao.merge(post1);
        post1 = em.find(Post.class, postExist.getId());

        //assert
        verify(em).merge(post1);
        assertEquals(expected, post1.getText());
    }
    //unit test with mockito
    @Test
    public void findTestWithMockedEm_postFind_success(){
        //arrange
        Post post1;

        //act
        post1 = postDao.find(postExist.getId());

        //assert
        verify(em).find(Post.class, postExist.getId());
        assertEquals(postExist.getId(),post1.getId());
    }
    //unit test with mockito
    @Test
    public void findTestWithMockedEm_postFind_returnNull() {
        //arrange
        Post post1;
        //act
        post1 = postDao.find(postNoExist.getId());
        //assert
        verify(em).find(Post.class,postNoExist.getId());
        assertNull(post1);
    }

    //unit test with mockito
    @Test
    public void findAllTest_postsAreInList_success(){
        //arrange
        List<Post> list1;
        list = new ArrayList<>();
        Post post1 = new Post();

        //act and assert
        post1.setId((long) 5);
        post1.setText("dddd");
        post1.setTitle("sss");

        list.add(postExist);
        list.add(post1);
        when(em.createQuery("SELECT post FROM Post post", Post.class)).thenReturn(query);
        when(query.getResultList()).thenReturn(list);

        list1 = postDao.findAll();

        verify(em).createQuery("SELECT post FROM Post post", Post.class);
        verify(query).getResultList();
        assertEquals(list,list1);
    }
}
