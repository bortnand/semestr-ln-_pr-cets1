package cvut.fel.dbs.lib.dao;

import cvut.fel.dbs.lib.model.Person;

import org.junit.*;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class PersonDaoTest {

    /////////////////////////////////////////////////////////////////////////////////////////////
    // Static fields and methods ////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////
    private static EntityManagerFactory emf;

    @BeforeClass
    public static void initEntityManagerFactory(){
        emf = Persistence.createEntityManagerFactory("test-pu");
    }

    @AfterClass
    public static void closeEntityManagerFactory(){
        emf.close();
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    // Instance fields and methods //////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////

    private EntityManager em;
    private PersonDao sut;


    @Before
    public void setUp() throws Exception {
        em = emf.createEntityManager();
        em.getTransaction().begin();

        sut = new PersonDao(em);
    }

    @After
    public void afterTest(){
        em.getTransaction().rollback();
        em.close();
    }

    //unit test without mockito
    @Test
    public void createTest_newUser_success() throws Exception {
        //arrange
        Person p = new Person();

        //act and assert
        sut.create(p);

        Assert.assertNotNull(p.getId());

        Person p2 = em.find(Person.class, p.getId());
        Assert.assertEquals(p.getId(), p2.getId());
        Assert.assertEquals(p.getName(), p2.getName());
    }
    //unit test without mockito
    @Test
    public void deleteTest_userExist_success() {
        //arrange
        Person p = new Person();

        //act and assert
        p.setName("Andy Bortnikov");
        p.setId((long)228);
        em.persist(p);
        assertTrue(em.find(Person.class,p.getId()) !=null);
        sut.delete(p);
        assertTrue(em.find(Person.class,p.getId()) ==null);
    }

    //unit test without mockito
    @Test
    public void mergeTest_userUpdated_success(){
        //arrange
        Person p = new Person();


        //act and assert
        p.setName("old name");
        em.persist(p);
        assertTrue(em.find(Person.class,p.getId()) !=null);
        p.setName("updated name");
        sut.merge(p);
        assertEquals("updated name", p.getName());
    }
    //unit test without mockito
    @Test
    public void findTest_userExist_success(){
        //arrange
        Person p = new Person();
        Person foundP;
        //act
        p.setName("Some name");
        em.persist(p);
        foundP = sut.find(p.getId());
        //assert
        assertTrue(foundP != null);
    }
    //unit test without mockito
    @Test
    public void findTest_userDoesntExist_returnNull() {
        //arrange
        Person p = new Person();
        Person foundP;
        //act
        p.setId((long)2212);
        p.setName("new post");
        foundP = sut.find(p.getId());
        //assert
        assertNull(foundP);
    }
    //unit test without mockito
    @Test
    public void findAllTest_3users_success(){
        //arrange
        Person p = new Person();
        Person p2 = new Person();
        Person p3 = new Person();
        List<Person> list;
        //act
        em.persist(p);
        em.persist(p2);
        em.persist(p3);
        list = sut.findAll();
        //assert
        assertEquals(3,list.size());
    }

}