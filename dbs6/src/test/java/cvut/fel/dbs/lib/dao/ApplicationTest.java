package cvut.fel.dbs.lib.dao;

import cvut.fel.dbs.lib.Application;
import cvut.fel.dbs.lib.model.Person;
import cvut.fel.dbs.lib.model.Post;
import cvut.fel.dbs.lib.view.NewJFrame;
import org.junit.*;
import org.mockito.Mockito;
import org.mockito.junit.MockitoRule;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.table.DefaultTableModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.powermock.api.mockito.PowerMockito.*;


public class ApplicationTest {

    private Application app;
    private EntityManager em;
    private static EntityManagerFactory emf;
    Post post;
    private NewJFrame frame;
    Person person;
    DateFormat df;


    @BeforeClass
    public static void initEntityManagerFactory(){
        emf = Persistence.createEntityManagerFactory("test-pu");
    }

    @AfterClass
    public static void closeEntityManagerFactory(){
        emf.close();
    }

    @Before
    public void setUp(){
        em = emf.createEntityManager();
        em.getTransaction().begin();
        df = new SimpleDateFormat("dd/MM/yy ");
        person = new Person();
        frame = new NewJFrame();

        app = mock(Application.class);

        doCallRealMethod().when(app).setup();
        doCallRealMethod().when(app).showPosts();
        doCallRealMethod().when(app).addPostToAnAuthor();
        doCallRealMethod().when(app).deletePost();
        doCallRealMethod().when(app).showAuthoredPosts();
        doCallRealMethod().when(app).deleteRelation();
        doCallRealMethod().when(app).deletePerson();
        doNothing().when(app).showDialog(anyString());
        app.setEmf(emf);
        app.setEm(em);

        post = new Post();
        post.setTitle("Updated Post");
        post.setText("abcdaabcda");
        post.setCategory("News");
        post.setDate(df.format(new Date()));

        person.setName("Andy");
        person.setEmail("affsf@fm.tt");
    }

    @After
    public void cleanUp() {
        try {
            frame.idPost_input.setText(post.getId().toString());
            app.deletePost();
            frame.userID_input1.setText(person.getId().toString());
            app.deletePerson();
        } catch (NullPointerException e) {

        }
        em.getTransaction().commit();
    }


    @Test
    public void addNewPostTest_postGiven_postAddedAndFound() {
        //arrange
        DefaultTableModel model;
        int rowCount;
        Post postFound;

        //act
        model = (DefaultTableModel) frame.tablePosts.getModel();

        doCallRealMethod().when(app).addNewPost(post,model);
        doCallRealMethod().when(app).refreshTablePost(model);

        app.showPosts();
        rowCount = model.getRowCount();
        app.addNewPost(post,model);
        postFound = app.postDao.find(post.getId());

        //assert
        assertEquals(rowCount+1, model.getRowCount());
        assertEquals(post.getTitle(),postFound.getTitle());
        assertEquals(post.getText(),postFound.getText());
        assertEquals(post.getCategory(),postFound.getCategory());
    }

    @Test
    public void addNewPostTest_postGiven_postNotAddedTitleLowerThan3() {
        //arrange
        DefaultTableModel model;
        Post postFound;

        //act
        model = (DefaultTableModel) frame.tablePosts.getModel();

        post.setTitle("ff");
        doCallRealMethod().when(app).addNewPost(post,model);
        doCallRealMethod().when(app).refreshTablePost(model);

        app.showPosts();
        app.addNewPost(post,model);
        postFound = app.postDao.find(post.getId());

        //assert
        Mockito.verify(app).showDialog("Title contains unsupported symbols or has wrong length(length must be 3 to 30)");
        assertNull(postFound);
    }
    @Test
    public void addNewPostTest_postGiven_postNotAddedTextLowerThan10() {
        //arrange
        DefaultTableModel model;
        Post postFound;

        //act
        model = (DefaultTableModel) frame.tablePosts.getModel();

        post.setText("ff");
        doCallRealMethod().when(app).addNewPost(post,model);
        doCallRealMethod().when(app).refreshTablePost(model);

        app.showPosts();
        app.addNewPost(post,model);
        postFound = app.postDao.find(post.getId());

        //assert
        Mockito.verify(app).showDialog("Text contains unsupported symbols or has wrong length(length must be 10 to 500)");
        assertNull(postFound);
    }

    @Test
    public void addAndUpdateTest_postGiven_successfullyAddedAndUpdated() {
        //arrange
        DefaultTableModel model;
        Post postFound;

        //act
        model = (DefaultTableModel) frame.tablePosts.getModel();

        doCallRealMethod().when(app).addNewPost(post,model);
        doCallRealMethod().when(app).updatePost(model);

        app.addNewPost(post,model);

        frame.idPost_input.setText(post.getId().toString());
        frame.titlePost_input.setText("New title");
        frame.textPost_input.setText("New textsgvdfvdfv");

        app.updatePost(model);
        postFound = app.postDao.find(post.getId());

        //assert
        assertEquals("New title",postFound.getTitle());
        assertEquals("New textsgvdfvdfv",postFound.getText());
        assertEquals(post.getId(),postFound.getId());
    }
    @Test
    public void addAndUpdateTest_postGiven_postAddedButNotUpdatedUnsupportedSymbols() {
        //arrange
        DefaultTableModel model;
        Post postFound;

        //act
        model = (DefaultTableModel) frame.tablePosts.getModel();

        doCallRealMethod().when(app).addNewPost(post,model);
        doCallRealMethod().when(app).updatePost(model);

        app.addNewPost(post,model);

        frame.idPost_input.setText(post.getId().toString());
        frame.titlePost_input.setText("New title");
        frame.textPost_input.setText("23423@@#(*&^%$#%^&*edfecdc");

        app.updatePost(model);
        postFound = app.postDao.find(post.getId());

        //assert
        Mockito.verify(app).showDialog("Text contains unsupported symbols or has wrong length(length must be 10 to 500)");
        assertNotNull(postFound);
        assertNotEquals("23423@@#(*&^%$#%^&*edfecdc",postFound.getText());
        assertEquals(post.getId(),postFound.getId());
        assertEquals(post.getText(),postFound.getText());
    }
    @Test
    public void addAndUpdateTest_postGiven_postAddedButNotUpdatedCategoryHigherThan20() {
        //arrange
        DefaultTableModel model;
        Post postFound;

        //act
        model = (DefaultTableModel) frame.tablePosts.getModel();

        doCallRealMethod().when(app).addNewPost(post,model);
        doCallRealMethod().when(app).updatePost(model);

        app.addNewPost(post,model);

        frame.idPost_input.setText(post.getId().toString());
        frame.titlePost_input.setText("New title");
        frame.textPost_input.setText("New teeeeeextdd");
        frame.categoryPost_input.setText("sssssssssssssslkjshlkrjbvslkdjfvbklsdjfvklsjdfvlskdjfnvjsldnv");

        app.updatePost(model);
        postFound = app.postDao.find(post.getId());

        //assert
        Mockito.verify(app).showDialog("Category contains unsupported symbols or has wrong length(length must be 3 to 20)");
        assertNotNull(postFound);
        assertNotEquals("New teeeeeextdd",postFound.getText());
        assertEquals(post.getId(),postFound.getId());
        assertEquals(post.getText(),postFound.getText());
        assertEquals(post.getCategory(),postFound.getCategory());
        assertNotEquals("sssssssssssssslkjshlkrjbvslkdjfvbklsdjfvklsjdfvlskdjfnvjsldnv",postFound.getCategory());
    }

    @Test
    public void postFoundAndDeleted_postGiven_postAddedFoundAndDeleted() {
        //arrange
        DefaultTableModel model;
        Post postFound;
        Post postDeleted;

        //act
        model = (DefaultTableModel) frame.tablePosts.getModel();

        doCallRealMethod().when(app).addNewPost(post,model);

        app.addNewPost(post,model);

        postFound = app.postDao.find(post.getId());

        frame.idPost_input.setText(post.getId().toString());

        app.deletePost();

        postDeleted = app.postDao.find(post.getId());

        //assert
        assertEquals(postFound.getText(),post.getText());
        assertEquals(postFound.getId(),post.getId());
        assertNull(postDeleted);
    }

    @Test
    public void postFoundAndDeleted_postGiven_postAddedButNotDeleted() {
        //arrange
        DefaultTableModel model;
        Post postFound;
        Post postDeleted;

        //act
        model = (DefaultTableModel) frame.tablePosts.getModel();

        doCallRealMethod().when(app).addNewPost(post,model);

        app.addNewPost(post,model);

        postFound = app.postDao.find(post.getId());

        frame.idPost_input.setText("32112");

        app.deletePost();

        postDeleted = app.postDao.find(post.getId());

        //assert
        Mockito.verify(app).showDialog("Post with given id doesn't exist");
        assertEquals(postFound.getText(),post.getText());
        assertEquals(postFound.getId(),post.getId());
        assertNotNull(postDeleted);
    }
    @Test
    public void createPersonAndPostAndShowAuthoredPostsAndAddPostToAnAuthor_successfullyShowedAndAdded() {
        //arrange
        int rowCount;
        Person foundAuthor;
        Post foundPost;
        DefaultTableModel model;

        //act
        model = (DefaultTableModel) frame.tablePosts2.getModel();

        doCallRealMethod().when(app).addNewPost(post,model);
        doCallRealMethod().when(app).refreshTablePost(model);

        app.addNewPost(post,model);
        app.personDao.create(person);

        frame.postID_input1.setText(post.getId().toString());
        frame.userID_input1.setText(person.getId().toString());

        app.showAuthoredPosts();
        rowCount = model.getRowCount();
        app.addPostToAnAuthor();

        foundAuthor = app.personDao.find(person.getId());
        foundPost = app.postDao.find(post.getId());
        app.showAuthoredPosts();

        //assert
        assertNotNull(foundAuthor);
        assertEquals(rowCount+1,model.getRowCount());
        assertEquals(1,foundAuthor.getAuthoredPosts().size());
        assertEquals(1,foundPost.getAuthors().size());
    }
    @Test
    public void createPersonAndPostAndShowAuthoredPostsAndAddPostToAnAuthor_successfullyShowedButNotAddedIdNotFound() {
        //arrange
        Person foundAuthor;
        Post foundPost;
        DefaultTableModel model;

        //act
        model = (DefaultTableModel) frame.tablePosts2.getModel();

        doCallRealMethod().when(app).addNewPost(post,model);
        doCallRealMethod().when(app).refreshTablePost(model);

        app.addNewPost(post,model);
        app.personDao.create(person);

        frame.postID_input1.setText(post.getId().toString());
        frame.userID_input1.setText("8898");

        app.showAuthoredPosts();
        app.addPostToAnAuthor();

        foundAuthor = app.personDao.find(person.getId());
        foundPost = app.postDao.find(post.getId());
        app.showAuthoredPosts();

        //assert
        Mockito.verify(app).showDialog("Author or post not found");
        assertNotNull(foundAuthor);
        assertEquals(0,foundAuthor.getAuthoredPosts().size());
        assertEquals(0,foundPost.getAuthors().size());
    }



}
